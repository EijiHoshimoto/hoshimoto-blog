+++
date = "2016-08-25T02:23:40+09:00"
draft = false
title = "何か書く"

+++

せっかくだから、静的コンテンツのみのWebサイトのビルド、デプロイを自動化した話でも書く。

<!--more-->

これとは別の個人サイト、以前はGithub Pagesでホストしていて、さくらVPSに立てたサーバーからリバースProxyでリクエストを飛ばしていたが、Hugoでサイトを作成しGithubにpushするとCircleCIでビルドしつつFabricでrsyncによって転送するという構成に変えた。具体的には下図のような構成に変更した。

{{< figure src="/images/dep1.png" >}}

ソースコードはGithubで管理しており、そちらにサイトのソースコード一式とCIに実行させる処理を記述したファイル (circle.yml) およびデプロイ手順を記したファイル (fabfile.py) をpushすると、CircleCIがいい感じにビルドからデプロイまで引き受けてくれる。

とても快適なので、こちらも更新するなら、同じような構成・手順にしたいけど、さすがにGithub + CircleCIは躊躇われるので、Gitのpost-receiveフックで頑張っている。さくらVPS上のobake.bizがGitでのバージョン管理、Webサーバーの役割を担っているのだから、post-receiveフックでpushされたコミットに含まれるソースコードをビルドして、/var/www配下の所定の場所にコピーすればgit pushでWebサイトの更新は自動化されるというわけである (下図の構成になっている)

{{< figure src="/images/dep2.png" >}}

~~~bash
#!/bin/sh

cd /home/hoshimoto/work/web || exit
unset GIT_DIR
git pull origin master
git submodule update --init
hugo --theme=projecthub
rsync -av public/ /var/web/hoshimoto/pages
~~~

今年に入って機械学習とIoTを始めるつもりで手を動かしはじめたけれども、時間が足りなすぎる。
