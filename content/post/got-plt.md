+++
date = "2016-09-02T18:33:36+09:00"
draft = false
title = "GOT overwriteとreturn into PLT"

+++

DEPとASLRが有効である場合、確かに攻撃の難易度は上がる。しかしながら、それらをテクニックも広く知られている。今回は初歩的な技法としてGOT Overwriteとreturn into PLTを紹介する。
<!--more-->

なお、簡単のため、ここではmain関数に直接GOT overwrite, return into PLTのコードを書きこむ (本来なら、indirect pointer overwriteやheap-based buffer overflowによって書き換える)

ここでの目標は、GOT overwriteで下のコードのprintf関数の呼び出しを、system関数の呼び出しに差し替えることとする。
(Buffer Overflowによる任意コード実行では、free関数など必ず呼び出されるであろう関数の呼び出しを、あらかじめ配置しておいたROPやシェルコードに差し替える)

```
#include <stdio.h>
#include <stdlib.h>

void hello(void) // この関数は呼び出されない
{
    system("echo hello");
}

int main(void)
{
    // ここのprintfがsystemに差し替えられると
    // シェルが立ち上がる
    printf("/bin/sh");
    return 0;
}
```

ひとまず、逆アセンブルします。

```
eiji@dev:~$ objdump -d hello | grep -b3 -a3 printf
831-
832-Disassembly of section .plt:
861-
862:08048310 <printf@plt-0x10>:
890- 8048310:   ff 35 f8 9f 04 08       pushl  0x8049ff8
939- 8048316:   ff 25 fc 9f 04 08       jmp    *0x8049ffc
989- 804831c:   00 00                   add    %al,(%eax)
1039-   ...
1044-
1045:08048320 <printf@plt>:
1068- 8048320:  ff 25 00 a0 04 08       jmp    *0x804a000
1118- 8048326:  68 00 00 00 00          push   $0x0
1162- 804832b:  e9 e0 ff ff ff          jmp    8048310 <_init+0x3c>
--
5847- 8048436:  c7 00 36 83 04 08       movl   $0x8048336,(%eax)
5904- 804843c:  b8 2b 85 04 08          mov    $0x804852b,%eax
5959- 8048441:  89 04 24                mov    %eax,(%esp)
6010: 8048444:  e8 d7 fe ff ff          call   8048320 <printf@plt>
6070- 8048449:  b8 00 00 00 00          mov    $0x0,%eax
6119- 804844e:  c9                      leave
6159- 804844f:  c3                      ret
```

ご存知の通り、libcの関数は実行時に動的リンクされ、実行時にアドレスが決定されます。call命令のオペランドを実行時に書き換える、すなわちコードを自己書き換えするようなプログラムでなければ、実行時に関数のアドレスを決定するなんてことはできないようにも思えるかもしれないが、自己書き換えコードなしに動的リンクを実現するために通常のLinuxのプログラムではPLT (Procedure Linkage Table) と GOT (Global Offset Table) というものを利用する。 (より正確には、ELFには.plt, .gotというセクションが含まれ、実行時に動的リンカーがこれらのセクションを利用することで動的リンクを実現しています)

上の逆アセンブルの結果からprintf@pltという関数のアドレスは実行前に決定しており、その値は0x08048320である。詳細は後述するがこのPLT上へのエントリーへと飛ぶ (上の例のprintfの場合は 0x08048320 printf@plt へと飛ぶ) ことによって、間接的に動的リンクしたprintf関数が呼び出されるようになっている。コードの自己書き換えを防ぐため、このPLT上のエントリーのアドレスはコンパイル時に決定し、実行毎に変わることはない。たとえば、 printf@plt のアドレスはいつでも同じであって、ASLRの影響を受けない。

printf@pltはjmp命令に始まり、jmp命令のオペランドは 0x0804a000 に書き込まれているアドレスとなっている。この 0x0804a000 はGOT (Global Offset Table) というテーブル上のアドレスであり、 0x0804a000 には実行時に動的リンカー (/lib/ld-linux.soです) が動的リンクしたprintf関数のアドレスを書き込みます。これによって、printf@pltから動的リンクしたprintf関数が呼び出されるようになります。

今回は、printfのGOTエントリー (0x0804a000) にsystem@pltのアドレスを書き込むことによって、printf関数の呼び出しをsystem@pltの呼び出し、すなわちsystem関数の呼び出しに差し替えてみます。なお、このようにGOTを書き換えることで任意の関数呼び出し時に任意のアドレスへ制御を移す (今回の場合はprintf関数呼び出し時にsystem@pltのアドレスへ制御を移す) テクニックをGOT overwriteといい、2000年代初頭からASLR回避のテクニックとして広く使われています。というのも、GOTのアドレスもまたコンパイル時に決定し、実行毎に変わることがないからです。

GOTに書き込むアドレスについてですが、上述の通り、system関数のアドレスはASLRによって実行するごとに変わってしまいます。そこでsystem@pltを利用します。これは、コンパイル時に決定しますので、system@pltへ制御を移すことによってASLRの影響を受けることなく目標を達成できます。

(このようにASLRの影響を排するために、PLTへと制御を移すテクニックをreturn into PLTといいます)

```
eiji@dev:~$ objdump -d hello | grep -b3 -a3 system
1118- 8048326:  68 00 00 00 00          push   $0x0
1162- 804832b:  e9 e0 ff ff ff          jmp    8048310 <_init+0x3c>
1222-
1223:08048330 <system@plt>:
1246- 8048330:  ff 25 04 a0 04 08       jmp    *0x804a004
1296- 8048336:  68 08 00 00 00          push   $0x8
1340- 804833b:  e9 d0 ff ff ff          jmp    8048310 <_init+0x3c>
--
5279- 8048415:  89 e5                   mov    %esp,%ebp
5328- 8048417:  83 ec 18                sub    $0x18,%esp
5378- 804841a:  c7 04 24 20 85 04 08    movl   $0x8048520,(%esp)
5435: 8048421:  e8 0a ff ff ff          call   8048330 <system@plt>
5495- 8048426:  c9                      leave
5535- 8048427:  c3                      ret
5575-
```

ご覧の通り、system@pltのアドレスは0x08048330ですので、main関数を以下のように書き換えればprintfでシェルが立ち上がる謎プログラムの完成です。

```
#include <stdio.h>
#include <stdlib.h>

void hello(void)
{
    system("echo hello");
}

int main(void)
{
    // printfのGOTエントリーをPLTのsystem関数のエントリーに書き換え
    *((int*) 0x0804a000) = 0x08048330;
    printf("/bin/sh");
    return 0;
}
```

なお、呼び出されることのないhello関数が何のためにあるかというと...

system関数を動的リンクする必要がない場合にはコンパイラはsystem@pltというエントリーを作らず、return into PLTでsystem@pltに移ることができません。今回は簡単のため、このような関数を定義してsystem@pltをコンパイラに書き込ませました。

system@pltのような扱いやすいPLTエントリーがない場合で、ASLR, DEPが有効な場合は、ROPガジェットというc9c3 (leave ret) などのret命令で終わる命令列を.textセクションから探しておいて、それをうまく継ぎ接ぎして任意の処理を実装します (Return Oriented Programming; ROPと言います) retはスタックにあるreturn addressのアドレスへジャンプする命令なので、スタックポインターを制御できる場合にはSSPも回避しつつ再現性のある攻撃を実装できます。
